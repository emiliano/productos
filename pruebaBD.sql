-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-03-2019 a las 20:07:41
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `saveProducto` (IN `nombre` VARCHAR(60), IN `precio` DECIMAL(8,2), IN `clvProd` VARCHAR(40), OUT `resultado` INT(11))  BEGIN
	DECLARE _total INT(11);
	
	SElect Count(id_producto) into _total from producto where clave_producto = clvProd;
	IF(_total >= 1) THEN
		SET resultado = -1; #La clave producto ya existe
	else
		INSERT INTO producto 
			(clave_producto, nombre, precio)
		VALUES 
			(clvProd, nombre, precio);
		SET resultado = LAST_INSERT_ID();
	END IF;
    END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id_producto` int(11) NOT NULL,
  `clave_producto` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `precio` decimal(8,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id_producto`, `clave_producto`, `nombre`, `precio`) VALUES
(1, 'clv_planta_agromex', 'Planta Agromex 2000', '2230.40'),
(2, 'tract_100', 'Tractor Diesel 1200', '1200.00'),
(3, 'ferti_olgss', 'Fertilizante', '231.00'),
(9, 'logitech', 'mause', '231.00'),
(10, 'clav6_prueba', 'Prueba', '123.20'),
(11, 'sony', 'audifonos', '7200.00'),
(12, 'uno_come', 'Unocome', '342.20');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_producto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
