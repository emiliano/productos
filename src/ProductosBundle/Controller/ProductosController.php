<?php

namespace ProductosBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use ProductosBundle\Entity\Producto;
use ProductosBundle\Form\ProductoType;
use Symfony\Component\HttpFoundation\Session\Session;


class ProductosController extends Controller
{
    //private $session;
    public function __construct(){
        //$this->session = new Session();
    }

    public function indexAction()
    {
        return $this->render('ProductosBundle:Default:index.html.twig');
    }

    
    public function showProductsAction(){
        $em = $this->getDoctrine()->getEntityManager();
        $productos = $em->getRepository("ProductosBundle:Producto")->findAll();


        return $this->render('ProductosBundle:Producto:listado.html.twig', 
                        array("productos" => $productos));
    }

    public function addProductoAction(Request $request){
        $p = new Producto();
        $form = $this->createForm(ProductoType::class, $p);
        $form->handleRequest($request);
        if($form->isSubmitted()){
            if($form->isValid()){
                $stts = $this->saveProduct($p->getNombre(), $p->getPrecio(),  $p->getClaveProducto());
                if($stts == -1){
                    $estatus = "Clave de producto repetida";
                    //$this->session->getFlashBag()->add("estatus", $estatus);
                }else{
                    if($stts > 0){
                        $estatus = "Producto agregado corectamente.";
                        //$this->session->getFlashBag()->add("estatus", $estatus);
                        return $this->redirectToRoute("listado");
                    }else {
                        $estatus = "No se agrego el producto.";
                        //$this->session->getFlashBag()->add("estatus", $estatus);
                    }
                }
            }
        }
        return $this->render('ProductosBundle:Producto:addproducto.html.twig', 
            array("form" => $form->createView()));
    }

    public function deleteProductoAction($id){
        $em = $this->getDoctrine()->getEntityManager();
        $producto = $em->getRepository("ProductosBundle:Producto")->find($id);
        $em->remove($producto);
        $flush = $em->flush();
        if($flush == null){
            $status = "Registro eliminado correctamente.";
        }else{
            $status = "Registro No eliminado.";
        }

        //$this->session->getFlashBag()->add("estatus", $estatus);
        return $this->redirectToRoute("listado");
    }

    public function editProductoAction(Request $request, $id){
        $em = $this->getDoctrine()->getEntityManager();
        $producto = $em->getRepository("ProductosBundle:Producto")->find($id);
        
        $form = $this->createForm(ProductoType::class, $producto);
        $form->remove('claveProducto'); 
        $form->handleRequest($request);

        if($form->isSubmitted()){
            if($form->isValid()){
                $em = $this->getDoctrine()->getEntityManager();
                $producto->setNombre($form->get("nombre")->getData());
                $producto->setPrecio($form->get("precio")->getData());
                $em->persist($producto);
                $flush = $em->flush();
                if($flush == null){
                    $estatus = "Producto editado corectamente.";
                    //$this->session->getFlashBag()->add("estatus", $estatus);
                    return $this->redirectToRoute("listado");
                }else{
                    $estatus = "No se edito el producto.";
                    //$this->session->getFlashBag()->add("estatus", $estatus);
                }
            }
        }
        return $this->render('ProductosBundle:Producto:editproducto.html.twig', 
            array("form" => $form->createView()));
    }

    private function saveProduct($nombre, $precio, $clvProd){
        $stts = 0;      
        $precio = (double)$precio;    
        $sql = "call saveProducto('$nombre', '$precio', '$clvProd', @stts)";
        $conn = $this->getDoctrine()->getManager()->getConnection();
        $stmt = $conn->prepare($sql);


        $stmt->execute();
        $stmt->closeCursor();
        $result = $conn->fetchAssoc("SELECT @stts");

        return (int)$result["@stts"];
    }
}
