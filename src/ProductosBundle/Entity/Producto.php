<?php

namespace ProductosBundle\Entity;

/**
 * Producto
 */
class Producto
{
    /**
     * @var integer
     */
    private $idProducto;

    /**
     * @var string
     */
    private $claveProducto;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $precio;


    /**
     * Get idProducto
     *
     * @return integer
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * Set claveProducto
     *
     * @param string $claveProducto
     *
     * @return Producto
     */
    public function setClaveProducto($claveProducto)
    {
        $this->claveProducto = $claveProducto;

        return $this;
    }

    /**
     * Get claveProducto
     *
     * @return string
     */
    public function getClaveProducto()
    {
        return $this->claveProducto;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Producto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set precio
     *
     * @param string $precio
     *
     * @return Producto
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return string
     */
    public function getPrecio()
    {
        return $this->precio;
    }
}

