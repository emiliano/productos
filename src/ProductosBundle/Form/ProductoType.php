<?php

namespace ProductosBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
class ProductoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('claveProducto', TextType::class, 
                        array("label" => "Clave Producto", 
                                "required" => "required",
                                "attr" => array("class" => "form-control")))
            ->add('nombre', TextType::class, 
                        array("label" => "Nombre", 
                                "required" => "required",
                                "attr" => array("class" => "form-control")))
            ->add('precio', TextType::class, 
                        array("label" => "Precio", 
                                "required" => "required",
                                "attr" => array("class" => "form-control"))) 
            ->add('Guardar', SubmitType::class, 
                        array("attr" => array("class" => "btn btn-info")));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ProductosBundle\Entity\Producto'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'productosbundle_producto';
    }


}
